package com.classpath.orders.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Component
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(InvalidOrderIdException.class)
    public ResponseEntity<Error> handleInvalidOrderId(Exception exception){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new Error(100, exception.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleInvalidOrder(MethodArgumentNotValidException exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new Error(200, exception.getLocalizedMessage()));
    }
}

@Data
class Error {
    private final int code;
    private final String message;
}