package com.classpath.orders.exception;

public class InvalidOrderIdException extends RuntimeException {

    public InvalidOrderIdException(String message){
        super(message);
    }

    @Override
    public String getMessage(){
        return super.getMessage();
    }
}