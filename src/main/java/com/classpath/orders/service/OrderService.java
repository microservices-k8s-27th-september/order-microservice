package com.classpath.orders.service;

import com.classpath.orders.exception.InvalidOrderIdException;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final Source source;

    public Order saveOrder(Order order){
        updateInventory(order);
        return this.orderRepository.save(order);
    }


    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order fetchOrderById(long orderId){
        return this.orderRepository.findById(orderId)
                    .orElseThrow(() -> new InvalidOrderIdException("Invalid order id "));
    }

    public void deleteOrder(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    private void updateInventory(Order order) {
        //push the message to the topic - pradeep-orders
        this.source.output().send(MessageBuilder.withPayload(order).build());
        log.info(" Published the order to the orders-topic on Kafka");
    }

    private Order fallBackOrder(Exception exception){
        log.error("Exception whil calling the inventory service :: {}", exception.getMessage());
        return Order.builder().orderId(1111).orderDate(LocalDate.now()).customerEmail("pradeep@gmail.com").customerName("Pradeep").build();

    }
}