package com.classpath.orders.model;

import lombok.*;
import org.springframework.web.bind.annotation.PathVariable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"orderId", "customerName", "customerEmail"})
//JPA Annotations
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @PastOrPresent(message = "Order date cannot be in the future")
    private LocalDate orderDate;

    @Min(value = 25000, message = "Minimum order value should be 25k")
    @Max(value = 75000, message = "Maximum order value cannot be greater than 75K")
    private double price;

    @NotEmpty(message = "Customer name cannot be empty")
    private String customerName;
    @NotEmpty(message = "Customer email cannot be empty")
    private String customerEmail;

    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    private Set<OrderLineItem> orderLineItemSet;

    //scaffolding method to set the links correctly on both sides of the relationship
    public void addOrderLineItem(OrderLineItem lineItem){
        if(this.orderLineItemSet == null){
            this.orderLineItemSet = new HashSet<>();
        }
        this.orderLineItemSet.add(lineItem);
        lineItem.setOrder(this);
    }
}