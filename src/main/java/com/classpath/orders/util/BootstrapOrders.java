package com.classpath.orders.util;

import com.classpath.orders.model.Order;
import com.classpath.orders.model.OrderLineItem;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import java.util.stream.IntStream;
import static java.time.ZoneId.systemDefault;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.stream.IntStream.range;

@Component
@RequiredArgsConstructor
public class BootstrapOrders implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Faker faker = new Faker();
        range(1, 10)
                .forEach((index )-> {
                    Order order = Order.builder()
                            .orderDate(faker.date().past(8, DAYS).toInstant().atZone(systemDefault()).toLocalDate())
                            .customerEmail(faker.internet().emailAddress())
                            .customerName(faker.name().fullName())
                            .price(faker.number().randomDouble(2,25000, 55000))
                            .build();
                    IntStream.range(1, faker.number().numberBetween(2,6)).forEach(i -> {
                        OrderLineItem lineItem = OrderLineItem.builder().price(faker.number().randomDouble(2,25000, 55000)).qty(faker.number().numberBetween(2,5)).build();
                        order.addOrderLineItem(lineItem);
                    });
                    orderRepository.save(order);
                });
    }
}